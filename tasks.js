// 1.Write a function add(3)(6), which returns the sum of these two numbers

var add = function(x) {
  return function(y) { 
    return x + y; 
  }
}

add(3)(6);

// 2.Find the average age of users.

const users = [
  {
    gender: 'male',
    age: 24,
  },
  {
    gender: 'female',
    age: 21,
  }, 
  {
    gender: 'male',
    age: 36,
  },
]

let getAverage = arr => {
  let reducer = (total, currentValue) => total + currentValue;
  let sum = arr.reduce(reducer);
  return sum / arr.length;
}

let ages = users.map(person => person.age);

console.log(getAverage(ages));

// 3. Write a function that gets an array and returns unique values with a new array
// For example:
// const result = findUniqueElements ([10, 5, 6, 10, 6, 7]);

function findUniqueElements(arr) {
  let unique = [];
  for (let i = 0; i < arr.length; i++) {
      for (j = 0; j < arr.length; j++) {
          if (i === j) {
              continue;
          } else if (arr[i] === arr[j]) {
              break;
          }
      }
      if (j === arr.length) {
          unique.push(arr[i]);
      }
  }
  
  return unique;
}
findUniqueElements([10, 5, 6, 10, 6, 7]); // result = [5, 7]



// 2-nd way solved the task about find unique elements:

function findUniqueElements(arr) {
  const result = [];
    arr.map((item, index) => {
      if (arr.indexOf(item) === arr.lastIndexOf(item)) {
        result.push(item);
      }
    })
  return result;
}
findUniqueElements([10, 5, 6, 10, 6, 7]); //result = [5, 7]


// 4.Create a createCounter function that returns one more value from each call and starts at 0

let createConter  = (function(n) { 
  return function () {
  n++;
  return n; 
  }
 }(-1)); 
const getCount = createConter;
console.log (getCount());  //0
console.log (getCount());  //1
console.log (getCount());  //2


